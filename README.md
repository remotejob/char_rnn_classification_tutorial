# char_rnn_classification_tutorial
https://github.com/pytorch/tutorials/tree/master/intermediate_source
https://pytorch.org/tutorials/intermediate/char_rnn_classification_tutorial.html


kaggle datasets create -p data/
cd data; zip libs.zip libs/; cd ..

kaggle datasets version --dir-mode -p data/ -m "Updated data"


kaggle kernels push -p train/
kaggle kernels status sipvip/charrnnclassificationtutorial
kaggle kernels output sipvip/charrnnclassificationtutorial -p output/
cp output/model0.pt data/model0.pt
kaggle datasets version -r zip -p data/ -m "Updated data 0"
kaggle datasets status sipvip/char_rnn_classification_tutorialdata


kaggle kernels push -p generator/
kaggle kernels status sipvip/charrnnclassificationtutorialgen
kaggle kernels output sipvip/charrnnclassificationtutorialgen -p output/

